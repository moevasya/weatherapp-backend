import mongoose, { Schema } from "mongoose";

//Create Schema

const signUpTemplate = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
    },

    password: {
      type: String,
      required: true,
    },
  },
  { timestamps: true },
);

const Info = mongoose.model("userInfo", signUpTemplate);
export default Info;
