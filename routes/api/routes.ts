import express, { response } from "express";
import Info from "../../models/signup";
import mongoose from "mongoose";
import weatherData from "../../utils/weather";

const router = express.Router();

const db = mongoose.connection;

router.post("/signup", (req, resp) => {
  const newUser = new Info({
    username: req.body.username,
    password: req.body.password,
  });
  newUser
    .save()
    .then((data) => {
      resp.json(200);
    })
    .catch((err) => {
      resp.json("not signed up");
      console.log(err);
    });
});

router.post("/login", (req, resp) => {
  const logInAttempt = new Info({
    username: req.body.username,
    password: req.body.password,
  });
  db.collection("userinfos").findOne(
    { username: `${req.body.username}` },
    (err, result) => {
      if (err) throw err;

      if (result) {
        if (
          result.username === req.body.username &&
          result.password === req.body.password
        ) {
          resp.json(200);
        } else {
          resp.json("please enter a valid username and password combination");
        }
      } else {
        resp.json("please enter a valid username and password combination");
      }
    },
  );
});

router.get("/Weather", (req, resp) => {
  const address: any = req.query.address;
  weatherData(address, (result:object) => {
    resp.json(result);
    console.log(result)
  });
});

export default router;
