import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import router from "./routes/api/routes";
import cors from "cors";
import Key from "./config/config";
import session from 'express-session'

const app = express();

//boddy parser middleware
app.use(bodyParser.json());

mongoose.connect(
  Key.mongoURI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log("databse connected2");
  },
);


app.use(express.json());
app.use(cors());
app.use("/app", router);

app.listen(5000, () => {
  console.log("server initialized successfully");
});
